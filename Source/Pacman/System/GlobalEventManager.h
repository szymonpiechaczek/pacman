// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GlobalEventManager.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class PACMAN_API UGlobalEventManager : public UObject
{
	GENERATED_BODY()
	
};
